package ru.toppink.restaurantservice.exception;

public class GeocodeException extends Exception {

    public GeocodeException(String message) {
        super(message);
    }

}
