package ru.toppink.restaurantservice;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import ru.toppink.restaurantservice.geocode.MapClient;

@Configuration
@EnableFeignClients(clients = {
        MapClient.class
})
public class FeignConfig {


}
