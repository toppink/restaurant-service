package ru.toppink.restaurantservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.toppink.restaurantservice.model.Restaurant;
import java.time.LocalTime;
import java.util.Set;

public interface RestaurantCustomRepository {

    Page<Restaurant> findByFilter(
            Double lat,
            Double lon,
            String city,
            String title,
            LocalTime time,
            Set<Boolean> open,
            Pageable pageable
    );

    Long findByFilterCount(
            Double lat,
            Double lon,
            String city,
            String title,
            LocalTime time,
            Set<Boolean> open
    );
}
