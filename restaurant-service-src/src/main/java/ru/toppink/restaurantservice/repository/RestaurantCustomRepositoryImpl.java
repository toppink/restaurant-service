package ru.toppink.restaurantservice.repository;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.FloatType;
import org.hibernate.type.StringNVarcharType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimeType;
import org.hibernate.type.TimestampType;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import ru.toppink.restaurantservice.model.Restaurant;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

@Component
@RequiredArgsConstructor
public class RestaurantCustomRepositoryImpl implements RestaurantCustomRepository {

    private final EntityManager em;

    private String restaurantsFilterSql;
    private String restaurantsFilterCountSql;

    @SneakyThrows
    @PostConstruct
    private void loadScripts() {
        // Load restaurants filter script
        restaurantsFilterSql = loadSql("scripts/restaurants_filter.sql");

        // Load restaurants filter count script
        restaurantsFilterCountSql = loadSql("scripts/restaurants_filter_count.sql");
    }

    @Override
    public Page<Restaurant> findByFilter(
            Double lat,
            Double lon,
            String city,
            String title,
            LocalTime time,
            Set<Boolean> open,
            Pageable pageable
    ) {
        if (restaurantsFilterSql == null)
            throw new NullPointerException("Sql script restaurantsFilterSql not found");

        Session session = em.unwrap(Session.class);
        NativeQuery<Restaurant> query = session.createNativeQuery(restaurantsFilterSql, Restaurant.class);
        query.setParameter("latitude", lat == null ? null : lat.floatValue(), FloatType.INSTANCE);
        query.setParameter("longitude", lon == null ? null : lon.floatValue(), FloatType.INSTANCE);
        query.setParameter("city", city, StringType.INSTANCE);
        query.setParameter("title", title, StringType.INSTANCE);
        query.setParameter("time", time == null ? null : Time.valueOf(time), TimeType.INSTANCE);
        query.setParameter("open", open);
        query.setParameter("limit", pageable.getPageSize());
        query.setParameter("offset", pageable.getOffset());

        List<Restaurant> resultList = query.getResultList();

        Long totalElements = findByFilterCount(lat, lon, city, title, time, open);

        return new PageImpl<>(resultList, pageable, totalElements);
    }

    @Override
    public Long findByFilterCount(
            Double lat,
            Double lon,
            String city,
            String title,
            LocalTime time,
            Set<Boolean> open
    ) {
        if (restaurantsFilterCountSql == null)
            throw new NullPointerException("Sql script restaurantsFilterCountSql not found");

        Session session = em.unwrap(Session.class);
        NativeQuery query = session.createSQLQuery(restaurantsFilterCountSql);
        query.setParameter("latitude", lat == null ? null : lat.floatValue(), FloatType.INSTANCE);
        query.setParameter("longitude", lon == null ? null : lon.floatValue(), FloatType.INSTANCE);
        query.setParameter("city", city, StringType.INSTANCE);
        query.setParameter("title", title, StringType.INSTANCE);
        query.setParameter("time", time == null ? null : Time.valueOf(time), TimeType.INSTANCE);
        query.setParameter("open", open);

        BigInteger result = (BigInteger) query.getSingleResult();
        return result == null ? null : result.longValue();
    }

    private String loadSql(String path) {
        try {
            InputStream inputStream = new ClassPathResource(path).getInputStream();

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                return reader.lines().collect(Collectors.joining(" "));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
