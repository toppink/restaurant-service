package ru.toppink.restaurantservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.toppink.restaurantservice.model.Restaurant;
import java.time.LocalTime;
import java.util.Set;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Long>, RestaurantCustomRepository {

    Page<Restaurant> findAll(Pageable pageable);

    @Override
    Page<Restaurant> findByFilter(
            Double lat,
            Double lon,
            String city,
            String title,
            LocalTime time,
            Set<Boolean> open,
            Pageable pageable
    );
}
