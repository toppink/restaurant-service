package ru.toppink.restaurantservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.restaurantservice.geocode.AddressDto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantCreateDto {

    @Size(max = 255)
    @NotBlank
    String title;

    @Size(max = 63)
    @NotBlank
    String phone;

    @Min(1)
    Double deliveryRadius = 3D;

    @Min(1)
    @Max(3)
    Integer costLevel = 2;

    @NotNull
    AddressDto address;
}
