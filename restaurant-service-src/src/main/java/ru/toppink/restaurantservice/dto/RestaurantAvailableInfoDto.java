package ru.toppink.restaurantservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class RestaurantAvailableInfoDto
{
    Boolean canCreateOrder;
    Integer timeToOpen;
}
