package ru.toppink.restaurantservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.restaurantservice.geocode.AddressDto;

import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantUpdateDto {
    String phone;
    Double deliveryRadius;
    Integer costLevel;
    LocalTime openingTime;
    LocalTime closingTime;
    Integer preventOrderTime;
    Boolean open;
    AddressDto address;
}
