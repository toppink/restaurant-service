package ru.toppink.restaurantservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantDto
{
    private Long id;
    private String title;
    private String phone;
    private String city;
    private String street;
    private String house;
    private Double deliveryRadius;
    private Integer costLevel;

    private LocalTime openingTime;
    private LocalTime closingTime;
    private Integer preventOrderTime;
    private Boolean open;

    private Double latitude;
    private Double longitude;
}
