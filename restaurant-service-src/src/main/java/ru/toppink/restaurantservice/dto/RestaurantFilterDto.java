package ru.toppink.restaurantservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.restaurantservice.geocode.AddressDto;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantFilterDto {

    private String title;
    private AddressDto address;
    private Set<Boolean> open = Set.of(true, false);
}
