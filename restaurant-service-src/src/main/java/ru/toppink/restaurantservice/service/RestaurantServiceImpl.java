package ru.toppink.restaurantservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import ru.toppink.restaurantservice.geocode.AddressDto;
import ru.toppink.restaurantservice.dto.RestaurantAvailableInfoDto;
import ru.toppink.restaurantservice.dto.RestaurantCreateDto;
import ru.toppink.restaurantservice.dto.RestaurantDto;
import ru.toppink.restaurantservice.dto.RestaurantFilterDto;
import ru.toppink.restaurantservice.dto.RestaurantUpdateDto;
import ru.toppink.restaurantservice.exception.GeocodeException;
import ru.toppink.restaurantservice.geocode.AddressInfoDto;
import ru.toppink.restaurantservice.geocode.GeocodeService;
import ru.toppink.restaurantservice.model.Restaurant;
import ru.toppink.restaurantservice.repository.RestaurantCustomRepositoryImpl;
import ru.toppink.restaurantservice.repository.RestaurantRepository;

import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.MINUTES;

@Slf4j
@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private RestaurantCustomRepositoryImpl restaurantCustomRepository;

    @Autowired
    private GeocodeService geocodeService;

    @Override
    public RestaurantDto createRestaurant(RestaurantCreateDto createDto) {
        Restaurant restaurant = new Restaurant();

        restaurant.setTitle(createDto.getTitle());
        restaurant.setDeliveryRadius(createDto.getDeliveryRadius());
        restaurant.setCostLevel(createDto.getCostLevel());
        restaurant.setPhone(createDto.getPhone());

        setAddressForRestaurant(restaurant, createDto.getAddress());

        Restaurant saved = restaurantRepository.save(restaurant);

        return convertToDto(saved);
    }

    @Override
    public Page<RestaurantDto> findByParams(RestaurantFilterDto filterDto, Pageable pageable) {
        AddressDto address = filterDto.getAddress();
        AddressInfoDto addressInfo;

        String city;
        Double latitude;
        Double longitude;
        LocalTime time;

        if (address != null) {
            try {
                addressInfo = geocodeService.geocode(address);
            } catch (GeocodeException e) {
                throw new HttpServerErrorException(HttpStatus.SERVICE_UNAVAILABLE, "Geocode service: " + e.getMessage());
            }

            city = addressInfo.getCity();
            latitude = addressInfo.getLatitude();
            longitude = addressInfo.getLongitude();
            String timezone = addressInfo.getTimezone();
            time = timezone != null ? OffsetTime.now(ZoneId.of(timezone)).toLocalTime().withNano(0) : null;
        } else {
            city = null;
            latitude = null;
            longitude = null;
            time = null;
        }

        String title = filterDto.getTitle();

        Page<Restaurant> page = restaurantRepository.findByFilter(
                latitude, longitude, city, title, time,
                filterDto.getOpen(), pageable
        );

        List<RestaurantDto> result = page.getContent().stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        return new PageImpl<>(result, page.getPageable(), page.getTotalElements());
    }


    @Override
    public RestaurantDto updateRestaurant(Long id, RestaurantUpdateDto updateDto) {
        Restaurant restaurant = restaurantRepository.findById(id).orElseThrow(
                () -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "Restaurant with id = " + id + " not found")
        );

        Optional.ofNullable(updateDto.getPhone()).ifPresent(restaurant::setPhone);
        Optional.ofNullable(updateDto.getDeliveryRadius()).ifPresent(restaurant::setDeliveryRadius);
        Optional.ofNullable(updateDto.getCostLevel()).ifPresent(restaurant::setCostLevel);
        Optional.ofNullable(updateDto.getOpeningTime()).ifPresent(restaurant::setOpeningTime);
        Optional.ofNullable(updateDto.getClosingTime()).ifPresent(restaurant::setClosingTime);
        Optional.ofNullable(updateDto.getOpen()).ifPresent(restaurant::setOpen);
        Optional.ofNullable(updateDto.getPreventOrderTime())
                .ifPresent(value -> restaurant.setPreventOrderTime(restaurant.getClosingTime().minusMinutes(value)));

        setAddressForRestaurant(restaurant, updateDto.getAddress());

        Restaurant saved = restaurantRepository.save(restaurant);

        return convertToDto(saved);
    }

    @Override
    public void deleteRestaurant(Long id) {
        Restaurant restaurant = restaurantRepository.findById(id).orElseThrow(
                () -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "Restaurant with id = " + id + " not found")
        );
        restaurant.setDeleted(true);
        restaurantRepository.save(restaurant);
    }

    @Override
    public RestaurantAvailableInfoDto isAvailable(Long id) {
        Restaurant restaurant = restaurantRepository.findById(id).orElseThrow(
                () -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "Restaurant with id = " + id + " not found")
        );

        boolean canCreateOrder;
        LocalTime now = LocalTime.now();
        LocalTime open = restaurant.getOpeningTime();
        LocalTime preventOrderTime = restaurant.getPreventOrderTime();

        canCreateOrder = restaurant.isOpen() && open.isBefore(now) && preventOrderTime.isAfter(now);

        Integer timeToOpen;
        if (!canCreateOrder && restaurant.isOpen()) {
            timeToOpen = (int) ((now.isAfter(open) ? 1440 : 0) + MINUTES.between(now, open));
        } else {
            timeToOpen = null;
        }

        return RestaurantAvailableInfoDto.builder()
                .canCreateOrder(canCreateOrder)
                .timeToOpen(timeToOpen)
                .build();
    }

    private void setAddressForRestaurant(Restaurant restaurant, AddressDto address) {
        if (address == null)
            return;

        restaurant.setCity(address.getCity());
        restaurant.setStreet(address.getStreet());
        restaurant.setHouse(address.getHouse());

        try {
            AddressInfoDto addressInfo = geocodeService.geocode(address);

            Integer quality = addressInfo.getQuality();
            if (quality > 1) {
                throw new HttpClientErrorException(HttpStatus.CONFLICT, "Address " + address.toString() + " not correctly");
            }

            Optional.ofNullable(addressInfo.getLatitude()).ifPresent(restaurant::setLatitude);
            Optional.ofNullable(addressInfo.getLongitude()).ifPresent(restaurant::setLongitude);
        } catch (GeocodeException e) {
            throw new HttpServerErrorException(HttpStatus.SERVICE_UNAVAILABLE, "Geocode exception: " + e.getMessage());
        }
    }

    private RestaurantDto convertToDto(Restaurant restaurant) {
        Integer preventOrderTime;

        if (restaurant.getPreventOrderTime() != null && restaurant.getClosingTime() != null) {
            preventOrderTime = (int) (MINUTES.between(restaurant.getPreventOrderTime(), restaurant.getClosingTime()));
        } else {
            preventOrderTime = null;
        }

        return RestaurantDto.builder()
                .id(restaurant.getId())
                .city(restaurant.getCity())
                .title(restaurant.getTitle())
                .phone(restaurant.getPhone())
                .street(restaurant.getStreet())
                .house(restaurant.getHouse())
                .deliveryRadius(restaurant.getDeliveryRadius())
                .costLevel(restaurant.getCostLevel())
                .closingTime(restaurant.getClosingTime())
                .openingTime(restaurant.getOpeningTime())
                .preventOrderTime(preventOrderTime)
                .latitude(restaurant.getLatitude())
                .longitude(restaurant.getLongitude())
                .open(restaurant.isOpen())
                .build();
    }
}

