package ru.toppink.restaurantservice.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.toppink.restaurantservice.dto.RestaurantAvailableInfoDto;
import ru.toppink.restaurantservice.dto.RestaurantCreateDto;
import ru.toppink.restaurantservice.dto.RestaurantDto;
import ru.toppink.restaurantservice.dto.RestaurantFilterDto;
import ru.toppink.restaurantservice.dto.RestaurantUpdateDto;
import ru.toppink.restaurantservice.model.Restaurant;

public interface RestaurantService {
    RestaurantDto createRestaurant(RestaurantCreateDto restaurant);

    RestaurantAvailableInfoDto isAvailable(Long id);

    Page<RestaurantDto> findByParams(RestaurantFilterDto filterDto, Pageable pageable);

    void deleteRestaurant(Long id);

    RestaurantDto updateRestaurant(Long id, RestaurantUpdateDto restaurant);
}
