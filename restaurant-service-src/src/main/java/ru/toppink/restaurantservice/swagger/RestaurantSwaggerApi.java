package ru.toppink.restaurantservice.swagger;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import ru.toppink.restaurantservice.dto.RestaurantAvailableInfoDto;
import ru.toppink.restaurantservice.dto.RestaurantCreateDto;
import ru.toppink.restaurantservice.dto.RestaurantDto;
import ru.toppink.restaurantservice.dto.RestaurantFilterDto;
import ru.toppink.restaurantservice.dto.RestaurantUpdateDto;

import javax.validation.Valid;

public interface RestaurantSwaggerApi {
    @ApiOperation(value = "Добавляем ресторан")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ресторан успешно добавлен"),
            @ApiResponse(code = 400, message = "Не удалось добавить ресторан")
    })
    @PostMapping("/private/api/v1/restaurant/create")
    RestaurantDto create(
            @Valid @RequestBody RestaurantCreateDto restaurantCreateDto
    );

    @GetMapping("/public/api/v1/restaurant/{id}/available")
    @ApiOperation(value = "Проверка возможности сделать заказ")
    RestaurantAvailableInfoDto isAvailable(
            @PathVariable Long id
    );

    @PostMapping("/public/api/v1/restaurant")
    @ApiOperation(value = "Вывод всех ресторанов по параметрам пользователю")
    Page<RestaurantDto> findByParams(
            @RequestBody RestaurantFilterDto filterDto,
            Pageable pageable
    );

    @DeleteMapping("/private/api/v1/restaurant/{id}")
    @ApiOperation(value = "Удаляем ресторан")
    void deleteRestaurant(
            @PathVariable Long id
    );

    @PatchMapping("/public/api/v1/restaurant/{id}")
    @ApiOperation(value = "Обновление информации о ресторане")
    RestaurantDto updateRestaurant(
            @PathVariable Long id,
            @RequestBody RestaurantUpdateDto updateDto
    );
}
