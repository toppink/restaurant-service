package ru.toppink.restaurantservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;
import org.springframework.cglib.core.GeneratorStrategy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalTime;

@Data
@Entity
@Table(name = "restaurant")
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "deleted = false")
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Size(max = 255)
    String title;

    @Size(max = 63)
    String phone;

    @Size(max = 63)
    String city;

    @Size(max = 127)
    String street;

    @Size(max = 63)
    String house;

    @Min(1)
    Double deliveryRadius;

    @Min(1)
    @Max(3)
    Integer costLevel;

    @NotNull
    boolean open;

    @NotNull
    boolean deleted;

    LocalTime openingTime;

    LocalTime closingTime;

    LocalTime preventOrderTime;

    Double latitude;

    Double longitude;
}
