package ru.toppink.restaurantservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;
import java.util.Map;

@Slf4j
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(HttpStatusCodeException.class)
    public ResponseEntity<Object> handleHttpClientException(HttpStatusCodeException ex) {
        String message = ex.getMessage();
        HttpStatus statusCode = ex.getStatusCode();

        Map<String, Object> body = Map.of(
                "message", message,
                "status", statusCode,
                "stack", ex.getStackTrace()
        );

        log.error("Exception: {}", body);
        return new ResponseEntity<>(body, statusCode);
    }
}
