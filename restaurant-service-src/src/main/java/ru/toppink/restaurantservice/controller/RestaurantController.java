package ru.toppink.restaurantservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;
import ru.toppink.restaurantservice.dto.RestaurantAvailableInfoDto;
import ru.toppink.restaurantservice.dto.RestaurantCreateDto;
import ru.toppink.restaurantservice.dto.RestaurantDto;
import ru.toppink.restaurantservice.dto.RestaurantFilterDto;
import ru.toppink.restaurantservice.dto.RestaurantUpdateDto;
import ru.toppink.restaurantservice.service.RestaurantService;
import ru.toppink.restaurantservice.swagger.RestaurantSwaggerApi;

import javax.validation.Valid;

@RestController
public class RestaurantController implements RestaurantSwaggerApi
{

    @Autowired
    private RestaurantService restaurantService;

    @Override
    public RestaurantDto create(RestaurantCreateDto restaurantCreateDto)
    {
        return restaurantService.createRestaurant(restaurantCreateDto);
    }

    @Override
    public RestaurantAvailableInfoDto isAvailable(Long id) {
        return restaurantService.isAvailable(id);
    }

    @Override
    public Page<RestaurantDto> findByParams(RestaurantFilterDto filterDto, Pageable pageable) {
        return restaurantService.findByParams(filterDto, pageable);
    }

    @Override
    public void deleteRestaurant(Long id) {
        restaurantService.deleteRestaurant(id);
    }

    @Override
    public RestaurantDto updateRestaurant(Long id, RestaurantUpdateDto updateDto) {
        return restaurantService.updateRestaurant(id, updateDto);
    }
}
