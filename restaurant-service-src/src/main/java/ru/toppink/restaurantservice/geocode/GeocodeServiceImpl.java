package ru.toppink.restaurantservice.geocode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.toppink.restaurantservice.exception.GeocodeException;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class GeocodeServiceImpl implements GeocodeService {

    private final MapClient mapClient;

    private static final String TOKEN = "895ffed94595b94f88c003423865549e69073111";
    private static final String SECRET = "3689ba11eeb274369fe544644ac18d9c4d53ecaa";

    @Override
    public AddressInfoDto geocode(AddressDto address) throws GeocodeException {
        String query = address.getCity() + ", " + address.getStreet() + ", " + address.getHouse();
        return geocode(query);
    }

    @Override
    public AddressInfoDto geocode(String address) throws GeocodeException {
        log.debug("Geocode - start: address = {}", address);
        List<AddressInfoDto> result;

        try {
            result = mapClient.getInfo(new String[]{address}, Map.of(
                    "Authorization", "Token " + TOKEN,
                    "X-Secret", SECRET
            ));
        } catch (Exception e) {
            throw new GeocodeException("Service not available");
        }

        if (result.size() != 0) {
            AddressInfoDto addressInfoDto = result.get(0);
            log.debug("Geocode - end: {}", addressInfoDto);
            return addressInfoDto;
        } else {
            throw new GeocodeException("Address is not correctly");
        }
    }
}
