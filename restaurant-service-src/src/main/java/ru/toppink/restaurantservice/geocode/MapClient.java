package ru.toppink.restaurantservice.geocode;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import java.util.Map;

@FeignClient(
        name = "map-client",
        url = "https://cleaner.dadata.ru"
)
@RequestMapping("/api/v1")
public interface MapClient {

    @PostMapping("/clean/address")
    List<AddressInfoDto> getInfo(
            @RequestBody String[] addresses,
            @RequestHeader Map<String, Object> headers
    );
}
