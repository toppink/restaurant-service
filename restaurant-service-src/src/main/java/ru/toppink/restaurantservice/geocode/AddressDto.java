package ru.toppink.restaurantservice.geocode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto {

    @NotBlank
    @Max(255)
    String city;

    @NotBlank
    @Max(255)
    String street;

    @NotBlank
    @Max(63)
    String house;

    @Max(63)
    String flat;

    public String toString() {
        return String.format("%s, %s, %s %s", city, street, house, flat);
    }
}
