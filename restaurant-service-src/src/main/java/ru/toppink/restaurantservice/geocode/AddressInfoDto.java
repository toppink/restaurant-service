package ru.toppink.restaurantservice.geocode;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressInfoDto {

    /**
     * Исходные данные
     */
    private String source;

    /**
     * Результативный адрес
     */
    private String result;

    /**
     * Страна
     */
    private String country;

    /**
     * Город
     */
    private String city;

    /**
     * Улица с ее типом (улица, проспект, шоссе)
     */
    @JsonProperty("street_with_type")
    private String streetWithType;

    /**
     * Улица
     */
    private String street;

    /**
     * Дом
     */
    private String house;

    /**
     * Чаосовой пояс
     */
    private String timezone;

    /**
     * Долгота
     */
    @JsonProperty("geo_lon")
    private Double longitude;

    /**
     * Широта
     */
    @JsonProperty("geo_lat")
    private Double latitude;

    /**
     * 0 - Точные координаты
     * 1 - Ближайший дом
     * 2 - Улица
     * 3 - Населенный пункт
     * 4 - Город
     * 5 - Координаты не определены
     */
    @JsonProperty("qc_geo")
    private Integer quality;
}
