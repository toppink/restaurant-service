package ru.toppink.restaurantservice.geocode;

import ru.toppink.restaurantservice.exception.GeocodeException;

public interface GeocodeService {

    AddressInfoDto geocode(AddressDto address) throws GeocodeException;

    AddressInfoDto geocode(String address) throws GeocodeException;
}
