select *
from (
		 select *,
				80.138 * sqrt((latitude - :latitude) ^ 2 +
							  ((longitude - :longitude) * cos((latitude + :latitude) * 0.00872664626)) ^ 2) as distance
		 from {h-schema}restaurant
	 )
		 as info
where (info.latitude is null or info.longitude is null or :latitude is null or :longitude is null or info.distance < info.delivery_radius)
  and (:title is null or info.title ilike :title)
  and (:city is null or info.city ilike :city)
  and (cast(:time as time) is null or (cast(:time as time) BETWEEN info.opening_time AND info.prevent_order_time))
  and ((:open) is null or info.open in (:open))
limit :limit offset :offset