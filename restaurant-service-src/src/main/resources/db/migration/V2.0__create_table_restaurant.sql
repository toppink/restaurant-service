create sequence hibernate_sequence;

create sequence restaurant_sequence;

create table restaurant(
    id int8 not null default nextval('restaurant_sequence'),
    title varchar (255) not null,
    phone varchar (63),
    city varchar (63),
    street varchar (127),
    house varchar (63),
    delivery_radius float,
    cost_level int,
    primary key (id)
)