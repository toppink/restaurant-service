alter table restaurant add column opening_time time;
alter table restaurant add column closing_time time;
alter table restaurant add column prevent_order_time time;
alter table restaurant add column latitude float;
alter table restaurant add column longitude float;